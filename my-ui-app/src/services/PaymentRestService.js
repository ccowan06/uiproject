import http from "../http-common";

export class PaymentRestService {

    getAllPayments() {
        return http.get("/all");
    }

    getStatus() {
        return http.get("/status");
    }

    rowcount() {
        return (http.get("/numberofrows").length) ? (http.get("/numberofrows")) : "0";
    }

    getbyid(id) {
        return (http.get(`/findbyid/${id}`));
    }

    getbytype(type) {
        return (type.length) ? http.get(`/findbytype/${type}`) : null;
    }

    addPayment(data) {
        return http.post("/", data);
    }

}

export default new PaymentRestService();