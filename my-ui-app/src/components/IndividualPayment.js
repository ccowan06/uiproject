import React, {Component} from 'react';
import '../styles/viewallpaymentscss.css';
import paymentByType from "../redux/actions/PaymentsByTypeActions";
import connect from "react-redux/lib/connect/connect";
import picture from "../scripts/commonScripts";

class IndividualPayment extends Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            try {
                var viewbyType = {paymentID: this.props.match.params.id, typesvsID: "ID"}
                console.log("the ID is: " + viewbyType.paymentID)
                this.props.dispatch(viewbyType)
            } catch (err) {
                console.log("no params passed: " + err)
                this.setState({error: true})
            }
        }
    }

    render() {
        const {paymentsByID} = this.props;
        console.log(this.props)
        return (
            <div>
                {(paymentsByID)?(
                <h1><b><u>More Details for Payment ID:</u></b> {paymentsByID.id}</h1>):<h1>Payment details page</h1>}
                <div className={"container"}>
                    {(paymentsByID)?(
                    <div className="card mb-3">
                        <img className="card-img" src={picture(paymentsByID.type)} alt="Card image" height={"50%"}
                             width={"50%"}/>
                        <div className="card-body">
                            <h5 className="card-title">Customer ID: {paymentsByID.custId}</h5>
                            <p className="card-text"><u><b>Payment Details</b></u><br/>
                                Amount: {paymentsByID.amount} <br/>
                                Date: {paymentsByID.paymentDate}<br/>
                                Type: {paymentsByID.type} <br/>
                            </p>
                        </div>
                    </div>): 0}
                </div>
            </div>

        )
    }
}

const mapDispatchToProps = (x) => (dispatch) => {
    return {
        dispatch: (x) => dispatch(paymentByType(x))
    };
};

const mapStateToProps = state => ({
    paymentsByID: state.paymentsbyID
})

export default connect(mapStateToProps, mapDispatchToProps)(IndividualPayment);