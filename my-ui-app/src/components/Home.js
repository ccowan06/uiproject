import React from 'react';
import image from '../images/shoptilldrop.jpg'

function Welcome(props) {
    const myStyle = {
        alignItems: "center",
        alignSelf: "center",
        textAlign: "center",
        display: "inline"
    };
    return (<main className={"bd-masthead"} id={"content"} role={"main"}>
        <div className={"container"}>
            <div className={"row"} style={myStyle}>
                <div>
                    <h1>Payment tracking site</h1>
                    <p>Please feel free to use this site to keep track of your purchases</p>
                </div>
            </div>
            <div className={"row"} style={myStyle}>
                <div>
                    <img className="img-fluid" src={image} alt="home image"/>
                </div>

            </div>
        </div>
    </main>)
}

export default Welcome;