import React, {Component} from 'react';
import connect from 'react-redux/lib/connect/connect';
import paymentList from '../redux/actions/PaymentListActios'
import '../styles/viewallpaymentscss.css'
import {Link} from "react-router-dom";
import picture from '../scripts/commonScripts'


class ViewAllPayments extends Component {

    constructor(props) {
        super(props)
        this.props.dispatch(paymentList())
       // this.state = {path: ""};
    }

    render() {
        const {payments, currentIndex} = this.props;
        console.log(this.props)

        return (<div>
            <div className={"container"}> {payments &&
            payments.map((item, index) => (
                <div className="card mb-3" key={index}>

                    <img className="card-img" src={picture(item.type)} alt="Card image" height={"50%"} width={"50%"}/>
                    <div className="card-body">
                        <h5 className="card-title">Customer ID: {item.custId}</h5>
                        <p className="card-text"><u><b>Payment Details</b></u><br/>
                            Amount: {item.amount} <br/>
                            Date: {item.paymentDate}<br/>
                            Type: {item.type} <br/>
                        </p>
                        <a tag={Link} href={`/payment/${item.id}`}>More Details</a>
                    </div>
                </div>
            ))}
            </div>
        </div>)
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    payments: state.payment
})

export default connect(mapStateToProps, mapDispatchToProps)(ViewAllPayments);