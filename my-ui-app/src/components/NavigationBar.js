import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import {Switch, Route, Link} from "react-router-dom";
import photo from '../payment.png'

function NavigationBar() {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top fixed-top">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="/home">Home <span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/about">About</a>
                        </li>
                        <li className="nav-item dropdown">

                            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Payments
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a tag={Link} href={"/viewpayments"} className="dropdown-item">
                                    View Payments
                                </a>
                                <a className="dropdown-item" tag={Link} href="/add">
                                    Add Payment</a>
                                <div className="dropdown-divider"></div>
                                <a className="dropdown-item" href="#">Update Payment</a>
                                <a className="dropdown-item" href="#">Delete Payment</a>
                            </div>
                        </li>
                    </ul>
                    <span className="navbar-text">
                        <img src={photo} alt={photo} height={'50px'}/>
                    </span>
                </div>
            </nav>
        </div>
    )
}

export default NavigationBar;