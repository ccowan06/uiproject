import React, {Component} from 'react';
import {Redirect} from "react-router-dom/index";
import connect from 'react-redux/lib/connect/connect';
import paymentList from '../redux/actions/PaymentListActios';
import PaymentRestService from "../services/PaymentRestService";

class Add extends Component {
    constructor(props) {
        super(props);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeCustID = this.onChangeCustID.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.savePayment = this.savePayment.bind(this);
        this.validation = this.validation.bind(this);
        this.checkSubmit = this.checkSubmit.bind(this);
        this.getMaxID = this.getMaxID.bind(this);
        this.props.dispatch(paymentList());

        this.state = {
            id: 0,
            custId: 0,
            amount: 0.00,
            type: "",
            paymentDate: "",
            valdationError: "",
            showHideError: false
        };
    }

    componentDidMount() {
        this.getMaxID();
        this.custID.focus()
    }

    getMaxID() {
        //max id in db + 1 for ID, auto populates so no conflicts in db
        const {payments} = this.props;

        let idArray = [0];
        let maxID = 0;
        let newID = 0;
        payments && payments.map((item, index) => (idArray[index] = item.id));
        console.log(idArray)
        maxID = idArray.reduce((function (a, b) {
            return Math.max(a, b)
        }))
        console.log(maxID)
        newID = maxID + 1;
        console.log(maxID + 1)

        //this.setState({id :newID})
        return newID;
    }

    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }

    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    onChangeCustID(e) {
        this.setState({
            custId: e.target.value
        });
    }


    onChangeDate(e) {
        this.setState({
            paymentDate: e.target.value
        });
    }

    savePayment() {
        var data = {
            id: this.getMaxID(),
            type: this.state.type,
            amount: this.state.amount,
            custId: this.state.custId,
            paymentDate: this.state.paymentDate
        }

        PaymentRestService.addPayment(data)
            .then(response => {
                this.setState({
                    submitted: true
                });

            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    validation() {
        let customerID = this.state.custId;
        let amount = this.state.amount;
        let type = this.state.type;
        console.log("Validation method")

        if (customerID == 0 || customerID.trim() == "") {
            this.setState({valdationError: "Please enter your Customer ID!"});
            return false;

        } else if (Number.isNaN((parseInt(amount))) || parseInt(amount) == 0 || amount.trim() == "") {
            this.setState({valdationError: "Please enter a valid numerical amount!"});
            return false;

        } else if (type.toString().length < 1) {
            this.setState({valdationError: "Please enter a payment type!"});
            return false;

        } else {
            return true;
        }
    }

    checkSubmit(e) {
        e.preventDefault();

        if (this.validation()) {
            alert("Form submitted");
            this.savePayment();
        } else {
            alert(this.state.valdationError);
            console.log(this.state.valdationError);
        }
    }


    render() {

        if (this.state.submitted) {
            return <Redirect to={"viewall"}/>
        }
        return (<form>

            <h1>Add Payment</h1>
            <div className="form-group">
                <label htmlFor="ID">Payment ID: </label>
                <input type="text" className="form-control" id="ID" aria-describedby="IDHelp"
                       placeholder={this.getMaxID()} value={this.getMaxID()} disabled={true}/>
            </div>

            <div className="form-group">
                <label htmlFor="custID">Customer ID: </label>
                <input type="text" className="form-control" id="custID" aria-describedby="custIDHelp"
                       placeholder="Enter your Customer ID" value={this.state.custId} onChange={this.onChangeCustID}
                       ref={input => this.custID = input}/>
            </div>

            <div className="form-group">
                <label htmlFor="paymentAmount">Payment amount: </label>
                <input type="text" className="form-control" id="paymentAmount" aria-describedby="amountHelp"
                       placeholder="Enter the amount of the payment" value={this.state.amount}
                       onChange={this.onChangeAmount}/>
            </div>

            <div className="form-group">
                <label htmlFor="paymentType">Payment type: </label>
                <input type="text" className="form-control" id="paymentType" aria-describedby="typeHelp"
                       placeholder="Enter the payment type" value={this.state.type} onChange={this.onChangeType}/>
            </div>

            <div className="form-group">
                <label htmlFor="paymentDate">Payment date: </label>
                <input type="date" className="form-control" id="paymentDate" aria-describedby="dateHelp"
                       value={this.state.paymentDate} onChange={this.onChangeDate}/>
            </div>
            <div className="form-group">
                <input type="submit" className="form-control" onClick={this.checkSubmit}/>
            </div>

        </form>)
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    payments: state.payment
})

export default connect(mapStateToProps, mapDispatchToProps)(Add);