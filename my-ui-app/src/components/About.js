import React, {Component} from "react";

export default class About extends Component {
    constructor(props) {
        super(props);
        this.state = {apistatus: "Uh oh!", rowcount: 0}
    }

    render() {
        const myStyle = {

            color: "purple",
            borderStyle: "groove",
            borderBlockColor: "blue"
        };

        return (
            <div>
                <br/>
                <section>
                    <h1 style={{color: "#b666d2"}}>Please see below for the status of our Payments API</h1>
                </section>
                <section>
                    <br/>
                    <br/>
                    <br/>
                    <p style={myStyle}>Status is: {this.state.apistatus}</p>
                    <p style={myStyle}>Your total number of payments so far is: {this.state.rowcount}</p>

                </section>
            </div>
        );
    }

    async componentDidMount() {
        let statusUrl = "http://localhost:8080/api/payments/status"
        let rowCountUrl = "http://localhost:8080/api/payments/numberofrows"
        let apiStatus = "Uh oh!"
        let rowCount = 0
        try {
            let responseStatus = await fetch(statusUrl);
            apiStatus = await responseStatus.text();
            console.log('The data is: ' + apiStatus)

            let responseRowCount = await fetch(rowCountUrl);
            rowCount = await responseRowCount.text()
            console.log('Row Count is: ' + rowCount)

        } catch (e) {
            console.log('The err is: ' + e.toString())
        }
        this.setState({apistatus: apiStatus, rowcount: rowCount})
    }
}
