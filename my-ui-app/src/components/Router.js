import {Route, Switch} from "react-router-dom";
import welcome from "./Home";
import about from "./About";
import ViewBy from "./ViewBy";
import add from "./Add";
import React from "react";
import individualPayment from './IndividualPayment'

function Router(){
    return (
        <div className="container mt-3">
            <Switch>
                <Route exact path={["/", "/home"]} component={welcome}/>
                <Route exact path="/about" component={about}/>
                <Route exact path="/viewpayments" component={ViewBy}/>
                <Route exact path="/add" component={add}/>
                <Route exact path="/payment/:id" component={individualPayment}/>
            </Switch>
        </div>
    )
}

export default Router;