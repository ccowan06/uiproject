import React, {Component} from "react";
import paymentByType from '../redux/actions/PaymentsByTypeActions'
import connect from "react-redux/lib/connect/connect";
import picture from '../scripts/commonScripts'

class ViewByType extends Component {
    constructor(props) {
        super(props);
        this.getTypes = this.getTypes.bind(this);
        this.handleChange = this.handleChange.bind(this);

        this.state = {
            path: "",
            paymentTypeArray: [0],
            selectedValue: "",
        }

        this.getTypes();
    }

    getTypes() {
        const {payments} = this.props;
        let paymentTypeArr = [];
        let uniqueTypes = [0];
        payments && payments.map((item, index) => (paymentTypeArr[index] = item.type.toString().toLowerCase()));

        uniqueTypes = Array.from(new Set(paymentTypeArr));
        return uniqueTypes;
    }

    handleChange(event) {
        this.setState({selectValue: event.target.value})
        console.log(event.target.value)
        let viewbyType = {paymenttype: event.target.value, typevsID: "type"}
        this.props.dispatch(viewbyType)
    }

    render() {

        const {paymentsByType, currentIndex} = this.props;
        console.log(this.props)

        return (
            <div>
                <div>
                    <select value={this.state.selectValue} onChange={this.handleChange}>
                        <option value="">Please choose a type</option>
                        {this.getTypes() && this.getTypes().map((x, y) => <option key={y}>{x}</option>)}</select>
                </div>
                <div className={"container"}> {paymentsByType &&
                paymentsByType.map((item, index) => (
                    <div className="card mb-3" key={index}>

                        <img className="card-img" src={picture(item.type)} alt="Card image" height={"50%"}
                             width={"50%"}/>
                        <div className="card-body">
                            <h5 className="card-title">Customer ID: {item.custId}</h5>
                            <p className="card-text"><u><b>Payment Details</b></u><br/>
                                Amount: {item.amount} <br/>
                                Date: {item.paymentDate}<br/>
                                Type: {item.type} <br/>
                            </p>
                        </div>

                    </div>
                ))}
                </div>
            </div>

        )
    }
}

const mapDispatchToProps = (x) => (dispatch) => {
    return {
        dispatch: (x) => dispatch(paymentByType(x))
    };
};

const mapStateToProps = state => ({
    payments: state.payment,
    paymentsByType: state.paymentsbyType
})

export default connect(mapStateToProps, mapDispatchToProps)(ViewByType);
