import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavigationBar from "./components/NavigationBar"
import {BrowserRouter} from "react-router-dom";
import Router from "./components/Router";

function App() {
    return (
        <div className="App">
            <header>
                <BrowserRouter>
                    <NavigationBar/>
                    <Router/>
                </BrowserRouter>
            </header>
        </div>
    );
}

export default App;
