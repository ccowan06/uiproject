import paypal from "../images/paypal.jpg";
import visa from "../images/1024px-Visa.svg.png";
import mastercard from "../images/mastercard.png";
import declined from "../images/credit-card-declined.png";

function picture(item) {
    let path = "";
    if (item) {
        switch (item.toString().toLowerCase()) {
            case "paypal":
                path = paypal;
                break;
            case "visa":
                path = visa;
                break;
            case "mastercard":
                path = mastercard;
                break;

            default:
                path = declined;
                break;
        }
        return path;
    }
    return declined;
}

export default picture;